//
//  main.m
//  RunTrainerIOS
//
//  Created by jarek on 24.04.2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "runtrainerAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([runtrainerAppDelegate class]));
    }
}
