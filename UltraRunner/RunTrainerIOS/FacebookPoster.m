//
//  FacebookPoster.m
//  Ultra runner
//
//  Created by jarek on 5/22/13.
//
//

#import "FacebookPoster.h"
#import "RunTrainerShared.h"

@implementation FacebookPoster

-(id) init
{
    if (self = [super init])
    {
        self.postParams =
        [[NSMutableDictionary alloc] initWithObjectsAndKeys:
         @"https://itunes.apple.com/us/app/ultra-running-endurance-trainer/id597872978?ls=1&mt=8", @"link",
         @"https://graph.facebook.com/368612103251728/picture", @"picture",
         @"Ultra running", @"name",
         @"Endurance trainer for runners", @"caption",
         [NSString stringWithFormat:NSLocalizedString(@"User has just got better in %@ workout!", nil), self.trainingName], @"description",
         @"",@"message",
         nil];
        [RunTrainerShared instance].session = [[FBSession alloc] init];
        [[RunTrainerShared instance].session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            [RunTrainerShared instance].session = session;
            [FBSession setActiveSession:session];
            // nothing to do here
        }];

    }
    return self;
}

-(void)postMessage:(int)action
{
    NSString* message;
    switch (action)
    {
        case kPostMessageActionGotBetter:
            message = NSLocalizedString(@"User has just got better in %@ workout!", nil);
            break;
        case kPostMessageActionFinishedWorkout:
            message = NSLocalizedString(@"User has just finished %@ workout!",nil);
    }
    
    [self.postParams setValue:[NSString stringWithFormat:message, self.trainingName] forKey:@"description"];
     if ([[FBSession activeSession]isOpen]) {    // Ask for publish_actions permissions in context
        if ([FBSession.activeSession.permissions
             indexOfObject:@"publish_actions"] == NSNotFound) {
            // No permissions found in session, ask for it
            [FBSession.activeSession
             requestNewPublishPermissions:
             [NSArray arrayWithObject:@"publish_actions"]
             defaultAudience:FBSessionDefaultAudienceFriends
             completionHandler:^(FBSession *session, NSError *error) {
                 if (!error) {
                     // If permissions granted, publish the story
                     [self publishStory];
                 }
             }];
        } else {
            // If permissions present, publish the story
            [self publishStory];
        }
    }else{
        /*
         * open a new session with publish permission
         */
        [FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                           defaultAudience:FBSessionDefaultAudienceFriends
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             [RunTrainerShared instance].session = session;
                                             [FBSession setActiveSession:session];
                                             if (!error && status == FBSessionStateOpen) {
                                                 [self publishStory];
                                             }else{
                                                 NSLog(@"error");
                                             }
                                         }];
    }

}
- (void)publishStory
{
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
              alertText =  @"There was an error publishing your post. Sorry!";
             [[[UIAlertView alloc] initWithTitle:@"Result"
                                         message:alertText
                                        delegate:self
                               cancelButtonTitle:@"OK!"
                               otherButtonTitles:nil]
              show];
          
         }
         // Show the result in an alert
     }];
}

@end
