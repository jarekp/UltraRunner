//
//  FacebookPoster.h
//  Ultra runner
//
//  Created by jarek on 5/22/13.
//
//

#import <Foundation/Foundation.h>
#define kPostMessageActionGotBetter 1
#define kPostMessageActionFinishedWorkout 2

@interface FacebookPoster : NSObject
@property (strong, nonatomic) NSMutableDictionary *postParams;
@property NSString* trainingName;

-(void)postMessage:(int)action;
@end
